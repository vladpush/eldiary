﻿namespace ElDiary.Entities
{
    public class ContactModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}