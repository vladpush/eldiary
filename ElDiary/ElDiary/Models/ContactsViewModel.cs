﻿using System.Collections.Generic;
using ElDiary.Entities;

namespace ElDiary.Models
{
    public class ContactsViewModel
    {
        public IEnumerable<Contact> Contacts;
    }
}