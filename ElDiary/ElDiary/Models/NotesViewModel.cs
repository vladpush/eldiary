﻿using System.Collections.Generic;
using ElDiary.Entities;

namespace ElDiary.Models
{
    public class NotesViewModel
    {
        public IEnumerable<NoteModel> Notes;
    }
}