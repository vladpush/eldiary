﻿namespace ElDiary.Models
{
    public class AddNoteViewModel
    {
        public string Date { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
    }
}